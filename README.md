# calControl

desktop app to control NI-VISA driver from webapp via API controls

## 1. HTTP GET Request: VisaResources/findall
### Response body formats
application/json, text/json
 
	[{
		alias: string;
		expandedUnaliasedName: string;
		interfaceNumber: number;
		interfaceType: HardwareInterfaceType;
		originalResourceName: string;
		resourceClass: string;
		opened: boolean;
	}]

	enum HardwareInterfaceType {
	
        //     A vendor-specific hardware interface.
		
        Custom = 0,
        //
        // Summary:
        //     A GPIB hardware interface.
        Gpib = 1,
        //
        // Summary:
        //     A VXI hardware interface.
        Vxi = 2,
        //
        // Summary:
        //     A GPIB hardware interface to a VXI hardware platform.
        GpibVxi = 3,
        //
        // Summary:
        //     A Serial hardware interface.
        Serial = 4,
        //
        // Summary:
        //     A PXI hardware interface.
        Pxi = 5,
        //
        // Summary:
        //     A TCPIP hardware interface.
        Tcp = 6,
        //
        // Summary:
        //     A USB hardware interface.
        Usb = 7
	}

### Sample:

	[{"alias":"MG3694C",
	"expandedUnaliasedName":"TCPIP0::192.168.0.254::inst0::INSTR",
	"interfaceNumber":0,
	"interfaceType":6,
	"originalResourceName":"TCPIP0::192.168.0.254::inst0::INSTR",
	"resourceClass":"INSTR",
	"opened":false},

	{"alias":"COM1",
	"expandedUnaliasedName":"ASRL1::INSTR",
	"interfaceNumber":1,
	"interfaceType":4,
	"originalResourceName":"ASRL1::INSTR",
	"resourceClass":"INSTR",
	"opened":false}]


## 2. HTTP POST Request VisaResources/open
### Request body format
application/json, text/json

	{ 
		ResourceName: string 
	}

### Sample:

	{ 
		ResourceName: "TCPIP0::192.168.0.254::inst0::INSTR"
	}

### Response body formats
application/json, text/json

	{
		data: string;
		description: string;
		success: boolean;
	}

### Samples:

	{
		data: null
		description: "ASRL50::INSTR opened"
		success: true
	}
	
	{
		data: null
		description: "An item with the same key has already been added."
		success: false
	}


## 3. HTTP POST Request VisaResources/close
### Request body format
application/json, text/json

	{ 
		ResourceName: string 
	}	

### Sample:

	{ 
		ResourceName: "TCPIP0::192.168.0.254::inst0::INSTR" 
	}

### Response body formats
application/json, text/json

	{
		data: string;
		description: string;
		success: boolean;
	}

### Samples:

	{
		data: null
		description: "TCPIP0::192.168.0.254::inst0::INSTR closed"
		success: true
	}

	{
		data: null
		description: "There is no open connection named TCPIP0::192.168.0.254::inst0::INSTR"
		success: false
	}


## 4. HTTP POST Request VisaResources/write
### Request body format
application/json, text/json

	{
		Message: string
		ResourceName: string,
	}

### Sample:

	{
		Message: "*idn?"
		ResourceName: "TCPIP0::192.168.0.254::inst0::INSTR"
	}

### Response body formats
application/json, text/json

	{
		data: string;
		description: string;
		success: boolean;
	}

### Samples:

	{
		data: null
		description: "5 bytes were written"
		success: true
	}

	{
		data: null
		description: "There is no open connection named TCPIP0::192.168.0.254::inst0::INSTR"
		success: false
	}


## 5. HTTP POST Request VisaResources/read
### Request body format
application/json, text/json

	{
		ResourceName: string
	}

### Sample:

	{ 
		ResourceName: "TCPIP0::192.168.0.254::inst0::INSTR"
	}

### Response body formats
application/json, text/json

	{
		data: string;
		description: string;
		success: boolean;
	}

### Sample:

	{
		data: "ANRITSU,MG3694C,133805,3.60"
		description: "Read 29 bytes"
		success: true
	}

	{
		data: null
		description: "Exception of type 'Ivi.Visa.IOTimeoutException' was thrown."
		success: false
	}